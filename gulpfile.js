'use strict';

const gulp = require('gulp'),
    fileinclude = require('gulp-file-include'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber'),
    removeComments = require('gulp-remove-html-comments'),
    postcss = require('gulp-postcss'),
    minify = require("gulp-csso"),
    sass = require('gulp-sass'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify-es').default,
    browserify = require('gulp-browserify'),
    sourcemaps = require('gulp-sourcemaps'),
    babel = require('gulp-babel'),
    autoprefixer = require('autoprefixer'),
    del = require('del'),
    browserSync = require('browser-sync').create(),
    mail = require('gulp-mail'),
    source = 'src/',
    build = 'public';


gulp.task('html', function () {
    return gulp.src([source + 'index.html'])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(removeComments())
        .pipe(gulp.dest(build))
        .pipe(browserSync.stream());
})
gulp.task('style', function () {
    return gulp.src(source + 'styles/main.scss')
        .pipe(sourcemaps.init())
        .pipe(plumber())
        .pipe(sass())
        .pipe(postcss([autoprefixer({
            grid: true,
            overrideBrowserslist: ['last 2 versions', 'ie 6-8', 'Firefox > 20']
        })]))
        .pipe(minify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(build + '/css'))
        .pipe(browserSync.stream());
})
gulp.task('script', function () {
    return gulp.src(source + 'js/index.js')
        .pipe(sourcemaps.init())
        .pipe(browserify())
        .pipe(uglify())
        .pipe(babel({
            presets: ['@babel/preset-env']
        }))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(build + '/js'))
        .pipe(browserSync.stream());
})
gulp.task('imagemin', function () {
    return gulp.src(source + 'images/**/*.{png,jpg,jpeg}')
        .pipe(imagemin([
            imagemin.mozjpeg({ quality: 75, progressive: true }),
            imagemin.optipng({ optimizationLevel: 5 })
        ]))
        .pipe(gulp.dest(build + '/images'));
})
gulp.task('browser-sync', function () {
    browserSync.init({
        server: {
            baseDir: './' + build
        }
    })
    gulp.watch(source + '**/*.html', gulp.series('html'));
    gulp.watch(source + 'styles/**/*.{scss,sass}', gulp.series('style'));
    gulp.watch(source + 'js/**/*.{js, json}', gulp.series('script'));
})
gulp.task('copy', function () {
    return gulp.src([
        source + 'fonts/**/*.{otf,ttf,eot,woff,woff2}'
    ], {
        base: './' + source
    })
        .pipe(gulp.dest(build));
})
gulp.task('clear', function () {
    return del(build);
})

gulp.task('public-files', gulp.series('clear', 'html', 'style', 'script', 'imagemin', 'copy', function (done) {
    done();
}))

gulp.task('do', gulp.series('public-files', 'browser-sync', function (done) {
    done();
}))