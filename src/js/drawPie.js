module.exports = function (canvas) {
  let context = canvas.getContext("2d"),
      percent = canvas.dataset.percent,
      color = canvas.dataset.color,
      lineWidth = 14;
  const deg = (percent * 360) / 100;

  context.beginPath();
  context.arc(canvas.width / 2, canvas.height / 2, canvas.width / 2 - lineWidth, 0, 360);
  context.lineWidth = lineWidth;
  context.strokeStyle = "#e8e8e8";
  context.stroke();
  context.closePath();
  context.beginPath();
  context.arc(canvas.width / 2, canvas.height / 2, canvas.width / 2 - lineWidth, 0, deg * (Math.PI / 180));
  context.lineWidth = lineWidth;
  context.lineCap = 'round';
  context.strokeStyle = `#${color}`;
  context.stroke();
  context.closePath();
};