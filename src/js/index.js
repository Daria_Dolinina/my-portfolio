'use strict';
const $ = require('jquery');
require('popper.js');
require('bootstrap');
const drawPie = require('./drawPie');

$(function () {
    if (!window.location.hash) {
        window.location.hash = $('section:first').attr('id')
    }
    let scrolling = false

    $(window).on('wheel', (function (event) {
        if (!scrolling && window.outerWidth >= 992) {
            scrolling = true
            let $currentSection = $(window.location.hash);
            if (event.originalEvent.deltaY < 0
                && $currentSection.prev().length) {                    
                window.location.hash = $currentSection.prev().attr('id')
                $(document).scrollTop($currentSection.prev().offset().top);
            }
            else if (event.originalEvent.deltaY > 0
                && $currentSection.next().length) {
                window.location.hash = $currentSection.next().attr('id')
                $(document).scrollTop($currentSection.next().offset().top);
            }

            setTimeout(function () {
                scrolling = false;
            }, 1000)
        }
    }))

    $('.progress-bar-pie').each(function () {
        drawPie(this);
        this.style.transform = 'rotate(-90deg)'
    });

    // $('#email').submit(function () {
    //     $.ajax({
    //         url: '../js/email.php',
    //         method: 'POST',
    //         dataType: 'html',
    //         data: $('#email').serialize(),
    //     })
    //         .done(function () {
    //             alert('Успех');
    //         })
    //         .fail(function () {
    //             alert('Неудача');
    //         });
    //     return false;
    // });

    $('#navbarToggler button').click(function () {
        $('#navbarToggler').collapse('hide')
    })
});