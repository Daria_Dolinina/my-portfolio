# Портфолио

### Запуск сборки

`npm run start`


### TODO list

:white_check_mark: ~~Переделать блок "Контакты"~~    
:white_check_mark: ~~Уменьшать размер изображений при сборке проекта~~    
:white_check_mark: ~~Сделать анимацию блока "Обо мне"~~    
:black_square_button: Сделать анимацию блока "Навыки"    
:white_check_mark: ~~Изменить фотографию в блоке "Обо мне"~~    
:white_check_mark: ~~Добавить скрины virtual-showcase в блоке "Работы"~~    
:black_square_button: Создать анимированный элемент на фон    
:black_square_button: Сделать рефакторинг    
:black_square_button: Добавить favicon    
:black_square_button: Перенести инфу о текущем лендиге в блок "Контакты"    
